# Usage of instructlab with new taxonomy


## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Infrasctructure](#Infrasctructure)
- [Usage](#usage)
- [Contact](#contact)


## Installation

Follow official documentation [[https://github.com/instructlab/instructlab]]

## Usage

First create a taxonomy entry like slurm

Check if it's correct

```
ilab diff
```

Do the lurning from taxonomy

```
ilab generate
```
When it finished, train (require lot's of memory)

```
ilab train --device=cuda
```
New model come here : models/ggml-model-f16.gguf

to get it smaller. push and convert / quantize with https://huggingface.co/spaces/ggml-org/gguf-my-repo or with llama.cpp convert-to-ggml

## Contact
drocc-crocc-contact-request <drocc-crocc-contact-request@groupes.renater.fr>
