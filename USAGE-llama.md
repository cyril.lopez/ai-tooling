# Using llama.cpp to manipulate model


## Table of Contents

- [Installation](#installation)
- [Usage](#usage)

## Installation

Pull the container :

```
podman pull ghcr.io/ggerganov/llama.cpp:full-cuda
```

## Usage

### Convert a model to ggml format

From a model with safetensors file, to a ggml format file :

```
podman run --device nvidia.com/gpu=all --name llama --security-opt=label=disable -it --rm -v /srv/containers/data/:/data ghcr.io/ggerganov/llama.cpp:full-cuda -c /data/merlinite-7b-lab-Q4_K_M-lurn_slurm/
```

### Quantize a model

From a ggml format file, to a quantized ggml format file, here in Q5_0:

```
podman run --device nvidia.com/gpu=all --name ilab --security-opt=label=disable -it --rm -v /opt/containers/data/:/data ghcr.io/ggerganov/llama.cpp:full-cuda -q /data/merlinite-7b-lab-Q4_K_M-lurn_slurm/ggml-model-f16.gguf Q5_0
```

To see all options:
```
podman run --device nvidia.com/gpu=all --name ilab --security-opt=label=disable -it --rm -v /opt/containers/data/:/data ghcr.io/ggerganov/llama.cpp:full-cuda -q  --help
```