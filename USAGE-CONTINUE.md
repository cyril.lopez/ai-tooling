# Using continue like copilot with ISDM Chat


## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Infrasctructure](#Infrasctructure)
- [Usage](#usage)
- [Contact](#contact)


## Installation

1. Install continue.dev plugin in VSCodium or VSCode
2. Create a JWT token on https://isdm-chat.crocc.meso.umontpellier.fr/ , click at bottum left on your login, Settings, Account and API keys
3. In your home in .continue/config.json set configuration from the file continue-config.json (in this git), set your token

## Usage

You can ask with "CTRL + i" part of code or just wait for auto-complete propossal.

## Infrasctructure

It's hosted on CROCC and in beta test and model use is codestral quantified.
Account are only approuved for institutional email.

## Contact
drocc-crocc-contact-request <drocc-crocc-contact-request@groupes.renater.fr>
