from langchain_core.messages import HumanMessage, SystemMessage
from langchain_core.prompts.chat import (
    ChatPromptTemplate,
    HumanMessagePromptTemplate,
    SystemMessagePromptTemplate,
)
from langchain_openai import ChatOpenAI
import sys, os

# Set the environment variables from shell environment
OPENAI_API_KEY = os.getenv("LLM_API_KEY")
OPENAI_CHAT_MODEL = os.getenv("LLM_MODEL")
OPENAI_CHAT_API_URL = os.getenv("LLM_API_URL")
OPENAI_CHAT_JWT_BEARER = os.getenv("LLM_JWT_BEARER")

inference_server_url = "http://localhost:8000/v1"

llm = ChatOpenAI(
    model=OPENAI_CHAT_MODEL,
    openai_api_key=OPENAI_API_KEY,
    openai_api_base=OPENAI_CHAT_API_URL,
)

if len(sys.argv) > 1:
    texte_question = sys.argv[1]
messages = [HumanMessage(content=texte_question)]
chat_model_response = llm.invoke(messages)
print(chat_model_response.content)
