import http from 'k6/http';
import { sleep, check } from 'k6';

export let options = {
  vus: 5,
  duration: '30s',
  //iterations: 1,
};

export default function () {
  const params = {
      headers: {
          'Authorization': 'Bearer ',
          'Content-Type': 'application/json',
      },
  };
  let response = http.post('https://isdm-chat.crocc.meso.umontpellier.fr/ollama/api/generate',
    JSON.stringify({
      model: 'mixtral:8x7b-instruct-v0.1-q5_0',
      prompt: 'Wrote a c code to find pi ?',
    }), params);

  check(response, {
    'status was 200': (r) => r.status == 200,
    'transaction time OK': (r) => r.timings.duration < 45000,
  });


  sleep(1);
}
